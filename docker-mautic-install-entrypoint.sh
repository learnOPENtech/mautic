#!/bin/bash

cd /srv/mautic
wait-for-it -h database -p 3306 -- su -s "/bin/bash" -c "php bin/console mautic:install -vvv --db_host=${DB_HOST} --db_port=${DB_PORT} --db_name=${DB_NAME} --db_user=${DB_USER} --db_password=${DB_PASSWORD} --admin_email=${ADMIN_EMAIL} --admin_password=${ADMIN_PASSWORD} --force ${HOST}" www-data && /docker-entrypoint.sh
