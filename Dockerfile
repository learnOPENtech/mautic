FROM registry.gitlab.com/learnopentech/np:ubuntu22.04 as build
ADD https://deb.nodesource.com/setup_20.x /tmp/nodesource.sh
RUN chmod u+x /tmp/nodesource.sh && /tmp/nodesource.sh
RUN apt-get update; DEBIAN_FRONTEND=noninteractive apt-get install -y \
    composer \
    nodejs \
    php-bcmath \
    php-curl \
    php-dom \
    php-gd \
    php-imap \
    php-intl \
    php-mysql \
    php-zip && apt-get clean && rm -rf /var/lib/apt/lists/*
RUN mkdir --mode=0770 /srv/mautic && chown www-data:www-data /srv/mautic && mkdir /tmp/mautic && chown www-data /tmp/mautic
USER www-data
RUN HOME=/tmp/mautic composer create-project mautic/recommended-project:^5 /srv/mautic --no-interaction

FROM registry.gitlab.com/learnopentech/np:ubuntu22.04
RUN apt-get update; DEBIAN_FRONTEND=noninteractive apt-get install -y \
    php-bcmath \
    php-curl \
    php-dom \
    php-gd \
    php-imap \
    php-intl \
    php-mysql \
    php-zip \
    wait-for-it && apt-get clean && rm -rf /var/lib/apt/lists/*
COPY --from=build --chown=www-data:www-data /srv/mautic /srv/mautic
ADD mautic.conf /etc/nginx/sites-available/mautic
RUN ln -s /etc/nginx/sites-available/mautic /etc/nginx/sites-enabled/mautic && \
    rm /etc/nginx/sites-enabled/default
ADD docker-mautic-install-entrypoint.sh /
ENTRYPOINT /docker-mautic-install-entrypoint.sh
RUN echo date.timezone = America/Chicago\\nmemory_limit = 512M >> /etc/php/8.1/fpm/php.ini && \
    echo date.timezone = America/Chicago\\nmemory_limit = 512M >> /etc/php/8.1/cli/php.ini
WORKDIR /srv/mautic
